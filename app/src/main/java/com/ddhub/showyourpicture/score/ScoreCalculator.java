package com.ddhub.showyourpicture.score;

import android.graphics.Bitmap;
import android.os.Looper;
import android.os.Message;

import com.ddhub.showyourpicture.BaseHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2015/10/6.
 */
public class ScoreCalculator {
    private HashMap<Bitmap, List<OnScoreCalculateListener>> mListenerMap;
    private ExecutorService mExecutors;

    private ResultHandler mResultHandler;

    public class ResultHandler extends BaseHandler<ScoreCalculator>{

        public ResultHandler(ScoreCalculator scoreCalculator, Looper looper) {
            super(scoreCalculator, looper);
        }

        @Override
        protected void handleMessage(ScoreCalculator scoreCalculator, Message msg) {
            int score = msg.what;
            Bitmap bitmap = (Bitmap) msg.obj;
            synchronized (ScoreCalculator.this) {
                List<OnScoreCalculateListener> listeners = mListenerMap.get(bitmap);
                if (listeners != null) {
                    for (OnScoreCalculateListener listener : listeners){
                        if (score == Integer.MIN_VALUE){
                            listener.onScoreCalculateFail(0, "");
                        }else {
                            listener.onScoreCalculateSucceed(score);
                        }
                    }
                    listeners.clear();
                }
                mListenerMap.remove(bitmap);
            }
        }
    }

    public ScoreCalculator(){
        mListenerMap = new HashMap<>();
        mExecutors = Executors.newFixedThreadPool(2);
        mResultHandler = new ResultHandler(this, Looper.getMainLooper());
    }

    public void startCalculate(OnScoreCalculateListener calculateListener, Bitmap bitmap){
        if (calculateListener == null || bitmap == null) {
            return;
        }
        if (mListenerMap.containsKey(bitmap)){
            mListenerMap.get(bitmap).add(calculateListener);
        }else {
            List<OnScoreCalculateListener> list = new ArrayList<>();
            list.add(calculateListener);
            mListenerMap.put(bitmap, list);
        }
        mExecutors.execute(new CalculateRunnable(bitmap));
    }

    public void cancel(Bitmap bitmap){
        synchronized (ScoreCalculator.this) {
            List<OnScoreCalculateListener> listeners = mListenerMap.get(bitmap);
            if (listeners != null) {
                listeners.clear();
            }
            mListenerMap.remove(bitmap);
        }
    }

    public void cancelAll(){
        for (Bitmap bitmap : mListenerMap.keySet()){
            if (bitmap != null) {
                List<OnScoreCalculateListener> listeners = mListenerMap.get(bitmap);
                if (listeners != null) {
                    listeners.clear();
                }
            }
        }
        mListenerMap.clear();
    }

    private class CalculateRunnable implements Runnable{
        Bitmap mBitmap;
        CalculateRunnable(Bitmap bitmap){
            mBitmap = bitmap;
        }

        @Override
        public void run() {
            if (mBitmap == null){
                return;
            }
            if (!mListenerMap.containsKey(mBitmap)){
                return;
            }

            int score = calculate(mBitmap);
            Message message = mResultHandler.obtainMessage(score, mBitmap);
            message.sendToTarget();
        }
    }

    private int calculate(Bitmap bitmap){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 100;
    }


}
