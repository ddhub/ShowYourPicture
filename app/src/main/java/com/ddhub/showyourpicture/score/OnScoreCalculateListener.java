package com.ddhub.showyourpicture.score;

/**
 * Created by Administrator on 2015/10/6.
 */
public interface OnScoreCalculateListener {

    void onScoreCalculateSucceed(int score);
    void onScoreCalculateFail(int error, String msg);

}
