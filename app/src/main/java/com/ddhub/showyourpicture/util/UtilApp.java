package com.ddhub.showyourpicture.util;

import android.content.Context;

import java.io.File;

/**
 * Created by Administrator on 2015/10/5.
 */
public class UtilApp {

    public static String getPictureFolder(Context context){
        if (context == null) {
            return "";
        }
        File file = context.getCacheDir();
        if (file == null) {
            return "";
        }
        return file.getAbsolutePath() + "/temp";
    }

    public static String generatePictureFile(Context context){
        String folder = getPictureFolder(context);
        File file = new File(folder);
        if (!file.exists()) {
            if (!file.mkdirs()){
                return "";
            }
        }
        return folder + "/" + System.currentTimeMillis();
    }

}
