package com.ddhub.showyourpicture;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * Created by Administrator on 2015/10/6.
 */
public abstract class BaseHandler<T> extends Handler {
    private WeakReference<T> mReference;

    public BaseHandler(T t){
        super();
        mReference = new WeakReference<T>(t);
    }

    public BaseHandler(T t, Looper looper){
        super(looper);
        mReference = new WeakReference<T>(t);
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        if (mReference.get() != null) {
            this.handleMessage(mReference.get(), msg);
        }
    }

    protected abstract void handleMessage(T t, Message msg);
}
