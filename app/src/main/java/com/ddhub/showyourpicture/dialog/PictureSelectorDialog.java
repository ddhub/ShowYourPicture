package com.ddhub.showyourpicture.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.ddhub.showyourpicture.R;

/**
 * Created by Administrator on 2015/10/5.
 */
public class PictureSelectorDialog extends DialogFragment {

    public interface OnPictureSelectedListener{
        void onPictureSelectCamera();
        void onPictureSelectPhoto();
    }

    private OnPictureSelectedListener selectedListener;

    public static PictureSelectorDialog createInstance(OnPictureSelectedListener selectedListener){
        PictureSelectorDialog dialog = new PictureSelectorDialog();
        dialog.selectedListener = selectedListener;
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity()).setItems(R.array.picture_selector, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (selectedListener == null) {
                    return;
                }
                if (which == 0){
                    selectedListener.onPictureSelectCamera();
                }else {
                    selectedListener.onPictureSelectPhoto();
                }
            }
        }).create();
    }
}
