package com.ddhub.showyourpicture;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.ddhub.showyourpicture.score.OnScoreCalculateListener;
import com.ddhub.showyourpicture.score.ScoreCalculator;
import com.ddhub.showyourpicture.util.UtilApp;
import com.ddhub.showyourpicture.view.ScannerView;

import java.io.File;

/**
 * Created by Administrator on 2015/10/5.
 */
public class PictureScoreActivity extends AppCompatActivity {

    private ScannerView mPictureImg;
    private TextView mScoreText;

    private Bitmap mCurrentBitmap;
    private String mTempPicturePath;
    private int mScore;

    private static final String INTENT_FROM = "intent_from";

    private static final int INTENT_CAMERA = 0;
    private static final int INTENT_PHOTO = 1;

    private static final int REQUEST_CAMERA = 1000;
    private static final int REQUEST_PICTURE = 1001;

    public static Intent createFromCamera(Context context) {
        Intent intent = new Intent(context, PictureScoreActivity.class);
        intent.putExtra(INTENT_FROM, INTENT_CAMERA);
        return intent;
    }

    public static Intent createFromPhoto(Context context) {
        Intent intent = new Intent(context, PictureScoreActivity.class);
        intent.putExtra(INTENT_FROM, INTENT_PHOTO);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_score);

        mPictureImg = (ScannerView) findViewById(R.id.picture_image);
        mPictureImg.setOnScanUpdateListener(mScanUpdateListener);
        mCurrentBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        mPictureImg.setImageBitmap(mCurrentBitmap);
        mPictureImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCalculate();
            }
        });

        mScoreText = (TextView) findViewById(R.id.picture_scoreText);

        int from = INTENT_CAMERA;
        Intent intent = getIntent();
        if (intent != null) {
            from = intent.getIntExtra(INTENT_FROM, INTENT_CAMERA);
        }
        if (from == INTENT_CAMERA) {
            startCamera();
        } else {
            startPhoto();
        }
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            mTempPicturePath = UtilApp.generatePictureFile(PictureScoreActivity.this);
            mTempPicturePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/temp";
            if (!TextUtils.isEmpty(mTempPicturePath)) {
                try {
                    File file = new File(mTempPicturePath);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void startPhoto() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            finish();
            return;
        }
        if (requestCode == REQUEST_CAMERA) {
            mCurrentBitmap = BitmapFactory.decodeFile(mTempPicturePath);
            mPictureImg.setImageBitmap(mCurrentBitmap);
        } else if (requestCode == REQUEST_PICTURE) {

        }
        startCalculate();
    }

    private void startCalculate() {
        mScoreText.setText("");
        new ScoreCalculator().startCalculate(new OnScoreCalculateListener() {
            @Override
            public void onScoreCalculateSucceed(int score) {
                mScore = score;
                mPictureImg.startScan();
            }

            @Override
            public void onScoreCalculateFail(int error, String msg) {
                mScore = 0;
            }
        }, mCurrentBitmap);
    }

    private ScannerView.OnScanUpdateListener mScanUpdateListener = new ScannerView.OnScanUpdateListener() {

        @Override
        public void onScanUpdate(int percent) {
            mScoreText.setText(String.valueOf(mScore * percent / 100));
        }
    };

}
