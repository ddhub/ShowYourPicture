package com.ddhub.showyourpicture;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ddhub.showyourpicture.dialog.PictureSelectorDialog;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tackPicture();
            }
        });
    }

    public void tackPicture(){
        PictureSelectorDialog.createInstance(new PictureSelectorDialog.OnPictureSelectedListener() {
            @Override
            public void onPictureSelectCamera() {
                startActivity(PictureScoreActivity.createFromCamera(MainActivity.this));
            }

            @Override
            public void onPictureSelectPhoto() {
                startActivity(PictureScoreActivity.createFromPhoto(MainActivity.this));
            }
        }).show(getFragmentManager(), "selector");
    }


}
