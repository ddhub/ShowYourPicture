package com.ddhub.showyourpicture.view;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

/**
 * Created by Administrator on 2015/10/6.
 */
public class ScannerView extends ImageView{

    private Bitmap mScanLine;

    private RectF mForegroundRect;
    private Paint mForegroundPaint;
    private ValueAnimator mScanAnimator;

    private OnScanUpdateListener mScanUpdateListener;

    private int mHeight;

    public interface OnScanUpdateListener{
        void onScanUpdate(int percent);
    }

    public ScannerView(Context context) {
        this(context, null);
    }

    public ScannerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScannerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        mForegroundRect = new RectF();
        mForegroundPaint = new Paint();
        mForegroundPaint.setColor(Color.argb(0x99, 0x77, 0x77, 0x77));
        mScanAnimator = ObjectAnimator.ofInt(0, getHeight());
        ValueAnimator.AnimatorUpdateListener mAnimationUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int top = (int) animation.getAnimatedValue();
                mForegroundRect.set(0, top, getWidth(), getHeight());
                invalidate();
                if (mScanUpdateListener != null) {
                    mScanUpdateListener.onScanUpdate(top * 100 / mHeight);
                }
            }
        };
        mScanAnimator.setDuration(2000);
        mScanAnimator.setInterpolator(new LinearInterpolator());
        mScanAnimator.addUpdateListener(mAnimationUpdateListener);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mHeight = getHeight();
        mForegroundRect.set(0, 0, getWidth(), mHeight);
        mScanAnimator.setIntValues(0, mHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(mForegroundRect, mForegroundPaint);
    }

    public void startScan(){
        if (!mScanAnimator.isStarted() && getHeight() != 0){
            mScanAnimator.start();
        }
    }

    public void stopScan(){
        mScanAnimator.cancel();
    }

    public void setOnScanUpdateListener(OnScanUpdateListener scanUpdateListener) {
        this.mScanUpdateListener = scanUpdateListener;
    }
}
